import { Component, ComponentFactoryResolver, EventEmitter, Input, OnInit, Output, ViewChild, ViewContainerRef } from '@angular/core';

@Component({
  selector: 'app-node',
  templateUrl: './node.component.html',
  styleUrls: ['./node.component.scss']
})
export class NodeComponent implements OnInit {
  
  @Input('name') nodeName :string='root';
  @Input('id') id :number|undefined;
  @Output('remove') destroy : EventEmitter<number> =new EventEmitter();
  nodeList:string[] = []
  ngOnInit(): void {
    
  }
  addChild(){
    let name=prompt('enter node name') || 'unknown';
    this.nodeList.push(name);
  }
  removeNode(index :number){
    this.nodeList.splice(index,1)
  }
  destroyNode(){
    this.destroy.emit(this.id);
  }

}
